<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Beranda - Website Desa Harjowinangun',
            'page' => 'v_beranda',
        ];
        return view('v_template', $data);
    }
}
