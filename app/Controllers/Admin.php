<?php

namespace App\Controllers;

class Admin extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Dashboard - Admin desa Harjowinangun',
            'page' => 'admin/v_dashboard',
        ];
        return view('admin/v_template_admin', $data);
    }
}
